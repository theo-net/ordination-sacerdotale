module.exports = {
  parser: '@babel/eslint-parser',
  parserOptions: {
    requireConfigFile: false,
  },
  extends: [
    'plugin:prettier/recommended',
    'eslint:recommended',
  ],
  globals: {
    "global": true,
    "$": true,
  },
  env: {
    amd: true,
    browser: true,
  },
  rules: {
    'max-len': ['error', {
      code: 250,
      ignoreUrls: true,
      ignoreComments: false,
      ignoreRegExpLiterals: true,
      ignoreStrings: false,
      ignoreTemplateLiterals: false,
    }],
  },
};
