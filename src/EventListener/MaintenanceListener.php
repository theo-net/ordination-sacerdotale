<?php
/**
 * @copyright Copyright (c) 2022 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Ordination Sacerdotale.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\EventListener;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Twig\Environment;

class MaintenanceListener
{
    public const MAINTENANCE_FILE = __DIR__.'/../../MAINTENANCE';
    public const IP_FILE = __DIR__.'/../../MAINTENANCE_IP';

    private Environment $twig;
    private array $ips = [];

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
        if (file_exists(self::IP_FILE)) {
            $ips = file_get_contents(self::IP_FILE);
            $this->ips = explode(';', str_replace("\n", ';', $ips ?: ''));
        }
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        if (file_exists(self::MAINTENANCE_FILE)
            && (!$this->ips || !in_array($_SERVER['REMOTE_ADDR'], $this->ips, false))) {
            $message = file_get_contents(self::MAINTENANCE_FILE);
            $template = $this->twig->render('maintenance.html.twig', ['message' => $message]);
            $event->setResponse(new Response($template, Response::HTTP_SERVICE_UNAVAILABLE));
            $event->stopPropagation();
        }
    }
}
