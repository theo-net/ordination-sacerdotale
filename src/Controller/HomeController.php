<?php
/**
 * @copyright Copyright (c) 2021 Grégoire OLIVEIRA SILVA <gregoire@theo-net.org>
 * @license AGPL-3.0
 *
 * This file is part of Ordination Diaconale.
 *
 * Zachée Association Enoria is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or any later version.
 *
 * Zachée Association Enoria is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero Public License for more details.
 *
 * You should have received a copy of the GNU Affero Public License
 * along with Enoria.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Form\NewsletterType;
use GuzzleHttp\Client;
use SendinBlue\Client\Api\ContactsApi;
use SendinBlue\Client\Configuration;
use SendinBlue\Client\Model\CreateContact;
use SendinBlue\Client\Model\CreateUpdateContactModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(Request $request): Response
    {
        $success = false;
        $error = false;

        $form = $this->createForm(NewsletterType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var string $apiToken */
            $apiToken = $this->getParameter('sendinblue_api');
            $config = Configuration::getDefaultConfiguration()->setApiKey(
                'api-key',
                $apiToken
            );
            $apiInstance = new ContactsApi(
                new Client(),
                $config
            );
            $createContact = new CreateContact();
            $createContact['email'] = $form->get('email')->getData();  // @phpstan-ignore-line
            $createContact['attributes'] = [  // @phpstan-ignore-line
                'NOM' => $form->get('lastname')->getData(),
                'PRENOM' => $form->get('firstname')->getData(),
            ];
            $createContact['listIds'] = [2];  // @phpstan-ignore-line

            try {
                $result = $apiInstance->createContact($createContact);
                if ($result instanceof CreateUpdateContactModel) {
                    $success = true;
                }
            } catch (\Exception $e) {
                $error = true;
            }
        }

        // Prayer block
        /**
         * @var null|string $prayerDate
         */
        $prayerDate = $this->getParameter('prayer_date');
        if (is_string($prayerDate)) {
            $diff = (int) ((new \DateTime($prayerDate))->format('Ymd')) - (int) ((new \DateTime())->format('Ymd'));
            $prayerShow = $diff >= 0;
        } else {
            $prayerShow = false;
        }

        return $this->render('home/index.html.twig', [
            'ordination_date' => $this->getParameter('ordination_date'),
            'youtube_id' => $this->getParameter('youtube_id'),
            'album_id' => $this->getParameter('album_id'),
            'cloud_id' => $this->getParameter('cloud_id'),
            'prayer_show' => $prayerShow,
            'prayer_date' => $prayerDate,
            'success' => $success,
            'error' => $error,
            'form' => $form->createView(),
            'new_name' => $this->isNewName(),
        ]);
    }

    /**
     * @Route("/legals", name="legals")
     */
    public function legals(): Response
    {
        return $this->render('home/legals.html.twig', [
            'new_name' => $this->isNewName(),
            'new_address' => $this->isNewAddress(),
        ]);
    }

    /**
     * @Route("/{locale}", name="language", requirements={"locale": "en|fr|he"})
     */
    public function language(string $locale, Request $request): Response
    {
        $request->setLocale($locale);
        $request->getSession()->set('_locale', $locale);

        $referer = $request->headers->get('referer');
        if (null !== $referer) {
            return $this->redirect($referer);
        }

        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/story", name="story")
     */
    public function story(): Response
    {
        return $this->render('home/story.html.twig');
    }

    /**
     * @Route("/ordination", name="ordination")
     */
    public function ordination(): Response
    {
        return $this->render('home/ordination.html.twig', [
            'youtube_id' => $this->getParameter('youtube_id'),
        ]);
    }

    /**
     * @Route("/reception", name="reception")
     */
    public function reception(): Response
    {
        return $this->render('home/reception.html.twig');
    }

    /**
     * @Route("/gifts", name="gifts")
     */
    public function gifts(): Response
    {
        return $this->render('home/gifts.html.twig', [
            'gifts' => [
                ['id' => '001', 'price' => 31],
                ['id' => '002', 'price' => 42],
                ['id' => '003', 'price' => 30],
                ['id' => '004', 'price' => 47],
                ['id' => '005', 'price' => 27],
                ['id' => '006', 'price' => 29.9],
                ['id' => '007', 'price' => 24.9],
                ['id' => '008', 'price' => 9.9],
                ['id' => '009', 'price' => 205],
                ['id' => '010', 'price' => 203.4],
                ['id' => '011', 'price' => 192.6],
                ['id' => '012', 'price' => 30],
                ['id' => '013', 'price' => 400],
                ['id' => '014', 'price' => 500],
            ],
            'new_name' => $this->isNewName(),
            'new_address' => $this->isNewAddress(),
        ]);
    }

    /**
     * @Route("/first-masses", name="first-masses")
     */
    public function firstMasses(): Response
    {
        return $this->render('home/first-masses.html.twig', [
            'masses' => [
                ['id' => '001', 'datetime' => new \DateTime('2022-06-27T18:30:00+0200')],
                ['id' => '003', 'datetime' => new \DateTime('2022-06-29T18:30:00+0200')],
                ['id' => '004', 'datetime' => new \DateTime('2022-06-30T18:30:00+0200')],
                ['id' => '006', 'datetime' => new \DateTime('2022-07-02T18:00:00+0200')],
                ['id' => '007', 'datetime' => new \DateTime('2022-07-03T10:30:00+0200')],
            ],
        ]);
    }

    protected function isNewName(): bool
    {
        /**
         * @var null|string $dateChangeName
         */
        $dateChangeName = $this->getParameter('date_change_name');
        if (is_string($dateChangeName)) {
            return (new \DateTime($dateChangeName)) < (new \DateTime());
        }

        return false;
    }

    protected function isNewAddress(): bool
    {
        /**
         * @var null|string $dateChangeAddress
         */
        $dateChangeAddress = $this->getParameter('date_change_address');
        if (is_string($dateChangeAddress)) {
            return (new \DateTime($dateChangeAddress)) < (new \DateTime());
        }

        return false;
    }
}
